FROM node:12.16.1

RUN mkdir -p /home/node/editoria/node_modules && chown -R node:node /home/node/editoria

WORKDIR /home/node/editoria

COPY package.json ./package.json
COPY yarn.lock ./yarn.lock
RUN chmod a+w ./yarn.lock

USER node

RUN yarn

COPY --chown=node:node . .

RUN yarn build

RUN chmod +x ./scripts/initialize-app.sh
