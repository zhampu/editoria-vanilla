import { css } from 'styled-components'

export default css`
  margin-left: auto;
  margin-right: auto;
  margin-top: 20px;
  width: 100px;
  img {
    filter: grayscale(100%);
    max-width: 100%;
  }
`
